<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $perPage = 20;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Store info users to database
     * 
     * @param object $request 
     */
    public function createUser($request)
    {
        $input = $request->except('password_confirmation');
        $input['password'] = Hash::make($input['password']);
        User::create($input);
    }

    /**
     * Get list users
     * 
     * @return Illuminate\Pagination\Paginator
     */
    public function getUsers()
    {
        return User::orderBy('mail_address')->paginate();
    }
}
