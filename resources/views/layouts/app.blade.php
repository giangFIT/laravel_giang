<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('title')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    @stack('style')
</head>
<body>
    <header>
        @include('partitions.header')
    </header>
    <main>
        <div class="main__content" style="margin:60px 0;">
            @yield('content')
        </div>
    </main>
    <footer class="text-center text-lg-start bg-light text-muted">
        @include('partitions.footer')
    </footer>
</body>
</html>
